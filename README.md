# drupal-custom-okd-project-template

Customizes the OKD4 project template for Drupal use case.
Similar to the [Drupal project template](https://gitlab.cern.ch/webservices/Drupal/Drupal-custom-okd-project-template)

This includes [console customizations](https://docs.okd.io/latest/web_console/customizing-the-web-console.html)
to provide [removal of default samples](https://docs.okd.io/latest/openshift_images/configuring-samples-operator.html)
since users don't have permission to use them in the Drupal cluster.